import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    name: string;
    password: string;

    constructor(
        private apiService: ApiService, 
        private router: Router, 
        private authService: AuthService
        ) {}

    ngOnInit() {
    }

    login() {
        // sendMessage returns the new data
        this.apiService.login(this.name, this.password)
            .subscribe(
                res => {
                    this.authService.setUserId(res);
                    this.router.navigate(['/chat'])
                },
                err => alert('Login falsch'),
            );
    }

}
