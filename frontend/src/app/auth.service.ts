import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    userId: number;
    constructor() {}


    getUserId(): number {
        // userId already set
        if (this.userId != undefined) {
            return this.userId;
        } else {
            //loopup if in localStoreage
            let stored = parseInt(localStorage.getItem('userId'));
            if (!isNaN(stored)) {
                this.userId = stored;
                return this.userId;
            } else {
                return NaN;
            }

        }
    }

    setUserId(userId: number): void {
        this.userId = userId;
        localStorage.setItem('userId', userId.toString());
    }
}
