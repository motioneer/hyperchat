import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {AuthService} from '../auth.service';
import {IMessage} from '../message.interface';
import {Router} from '@angular/router';
import {interval, Observable} from 'rxjs';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
    messages: IMessage[];
    newMessage: string = "";
    timer: Observable<any>;
    constructor(
        private apiService: ApiService,
        private router: Router,
        private authService: AuthService
    ) {
    }

    ngOnInit() {
        // get messages on init
        this.getMessages();
        this.timer = interval(5000);
        this.timer.subscribe(e => {
            this.getMessages()
        });
    }

    getMessages() {
        if (this.authService.getUserId()) {
            this.apiService.getMessages()
                .subscribe(
                    data => {
                        this.messages = data
                        console.log(this.messages);
                    },
                    err => this.router.navigate(['/login']),
                );
        } else {
            this.router.navigate(['/login']);
        }

    }

    sendMessage() {
        if (!this.newMessage)
            return;
        if (this.authService.getUserId()) {
            // sendMessage returns the new data
            this.apiService.sendMessage(this.newMessage)
                .subscribe(
                    data => {
                        this.messages = data
                        console.log(this.messages);
                        window.scrollTo(0, document.body.scrollHeight);
                    },
                    err => this.router.navigate(['/login']),
                );
            // reset newMessage field
            this.newMessage = "";
        } else {
            this.router.navigate(['/login']);
        }
    }


}
