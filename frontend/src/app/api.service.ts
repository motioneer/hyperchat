import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, of, from} from 'rxjs';
import {map} from 'rxjs/operators';
import {IMessage} from './message.interface';
import {AuthService} from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    backendUrl = "http://localhost:8000/api";

    constructor(private http: HttpClient, private authService: AuthService) {}

    getMessages(): Observable<IMessage[]> {
        const params = new HttpParams().set("userId", this.authService.getUserId().toString());
        return this.http.get<IMessage[]>(this.backendUrl + '/messages', {params: params });
    };

    sendMessage(message: string): Observable<IMessage[]> {
        const body = JSON.stringify({
            content: message,
            userId: this.authService.getUserId()
        });
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.http.post<IMessage[]>(this.backendUrl + '/message', body, {headers: headers});
    }

    login(name: string, password: string): Observable<any> {
        const body = JSON.stringify({
            name: name,
            password: password,
            userId: this.authService.getUserId()
        });
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.http.post<IMessage[]>(this.backendUrl + '/login', body, {headers: headers});
    }

}
