import {IUser} from './user.interface';

export interface IMessage {
   id: number;
   content: string;
   createdAt: Date;
   updatedAt: Date;
   user: IUser;
}