<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Messages;
use App\User;

class MessageController extends BaseController {

    public function getMessages(Request $request) {
        if ($request->input('userId') != '') {
            $messages = Messages::with('user')->get();
            return response()->json($messages, 200);
        } else {
            return response()->json(['error' => 'Not logged in'], 401);
        }
    }

    public function postMessage(Request $request) {
        if ($request->input('userId') != '') {
            $content = $request->input('content');
            $userId = $request->input('userId');
            $user = User::find($userId);
            if (!$content || !$user) {
                return response()->json(['error' => 'No content or user'], 403);
            }

            $message = new Messages();
            $message->content = $content;
            $message->user_id = $user->id;
            $message->save();
            return $this->getMessages($request);
        } else {
            return response()->json(['error' => 'Not logged in'], 401);
        }
    }

}
