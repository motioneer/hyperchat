<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller {

    public function login(Request $request) {
        $name = $request->input('name');
        $password = $request->input('password');

        $user = User::where('name', $name)->first();
        if ($user && password_verify($password, $user->password)) {
            // sets auth cookie for 60 minutes
            return response($user->id);
                          
        } else {
            return response()->json(['error' => 'Login incorrect'], 401);
        }
    }

}
