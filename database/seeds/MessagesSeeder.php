<?php

use Illuminate\Database\Seeder;
use App\Messages;

class MessagesSeeder extends Seeder {

    public function run() {
        DB::table('messages')->delete();

        Messages::create(array(
            'content' => 'Hast du diesen Valentin Sam gesehen?',
            'user_id' => 1
        ));

        Messages::create(array(
            'content' => 'Ja, der schaut mir aber gut aus.',
            'user_id' => 2
        ));

        Messages::create(array(
            'content' => 'So einen guten Entwickler habe ich schon lange nicht mehr gesehen!',
            'user_id' => 1
        ));

        Messages::create(array(
            'content' => 'BTW, hat irgendwer einen goldenen Ring gesehen, ich glaub ich hab meinen..',
            'user_id' => 3
        ));
        Messages::create(array(
            'content' => 'Nope.',
            'user_id' => 2
        ));
        Messages::create(array(
            'content' => 'Nein.',
            'user_id' => 1
        ));
        Messages::create(array(
            'content' => 'Keine Ahnung wovon du sprichst.',
            'user_id' => 1
        ));
         Messages::create(array(
            'content' => 'Oooookay....?',
            'user_id' => 3
        ));
    }

}
