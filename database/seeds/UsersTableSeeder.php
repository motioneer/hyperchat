<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->delete();

        User::create(array(
            'id' => 1,
            'name' => 'Gandalf',
            'email' => 'wizard@gmail.com',
            'password' => bcrypt('MagikRulez')
        ));

        User::create(array(
            'id' => 2,
            'name' => 'Frodo',
            'email' => 'smolboy@gmail.com',
            'password' => bcrypt('2ndBreakfast')
        ));

        User::create(array(
            'id' => 3,
            'name' => 'Sauron',
            'email' => 'ponys@evil.com',
            'password' => bcrypt('AugentropfenFreak')
        ));
    }

}
