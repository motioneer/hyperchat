# Willkommen bei Hyperchat!

Dies ist eine Beispielapplikation für die Verwendung von Angular 8 und PHP.


# Requirements

 - PHP ^7.2 - https://www.php.net/downloads
 - Composer - https://getcomposer.org/download/
 - Laravel ^6  - https://laravel.com/docs/master
 - mySQL - https://www.apachefriends.org/de/index.html
 - nodeJs ^12 - https://nodejs.org/en/download/
 - npm ^6 - https://www.npmjs.com/get-npm

# Installation
## 1)  **Datenbank konfigurieren**
 Im root Folder liegt die *.env* Datei. In dieser müssen folgende Attribute auf die lokale mySQL Datenbank angepasst werden:

1. DB_HOST
2. DB_PORT
3. DB_DATABASE
4. DB_USERNAME
5. DB_PASSWORD

## 2) **Backend starten**
In einer Command Line den root Folder navigieren und folgende Commands ausführen:
```php
composer install
php artisan migrate:refresh --seed
php artisan serve
```
Die Command Line zeigt nun die IP und Port an.

## 3) **Frontend konfigurieren**
Sollte die Backend IP und Port abweichen von **localhost:8000** oder **127.0.0.1:8000** muss die Frontend Konfiguration angepasst werden.

Diese Backend IP und Port müssen im Frontend angepasst werden, im File:
**/frontend/src/app/api.service.ts**

```javascript
backendUrl = "http://localhost:8000/api";
```
Hier **localhost:8000** mit der tatsächlichen IP und Port ersetzten.
Der **/api** Teil der URL muss dabei erhalten bleiben!

## 4) **Frontend starten**
In einer separaten Command Line in den **/frontend** Folder navigieren und folgende Commands ausführen:
```
npm install
ng serve
```
Die Command Line gibt nun die Adresse aus unter der das Frontend läuft.

# Benutzung
Im Browser zu der Frontendadresse navigieren.
Folgende Benutzer sind vordefiniert:

Benutzer| Passwort
------------ |------------
Gandalf   | MagikRulez
Frodo     | 2ndBreakfast   
Sauron  | AugentropfenFreak     

Der Chat kann jetzt genutzt werden.
Um **mehrere Benutzer** zu testen, öffne ein zweites Browserfenster im Inkognito Modus, und verwende einen anderen Benutzer.

Um **Benutzer zu wechseln** kann einfach wieder zu **/login** navigiert werden und erneut einloggen.

Um **ganz Auszuloggen**, muss der LocalStorage für die Seite gelöscht werden und die Seite neu laden.